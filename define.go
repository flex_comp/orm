package orm

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
)

type models struct {
	data map[string][]interface{}
}

func NewModels() *models {
	return &models{data: make(map[string][]interface{})}
}

func (m *models) Set(dsn string, model ...interface{}) {
	m.data[dsn] = model
}

func (m *models) randDsn() (dsn string) {
	for s := range m.data {
		dsn = s
		return
	}

	return ""
}

func (m *models) toDBResolver() *dbresolver.DBResolver {
	var dbr *dbresolver.DBResolver
	for k, v := range m.data {
		if dbr == nil {
			dbr = dbresolver.Register(
				dbresolver.Config{Sources: []gorm.Dialector{mysql.Open(k)}},
				v...,
			)

			continue
		}

		dbr.Register(
			dbresolver.Config{Sources: []gorm.Dialector{mysql.Open(k)}},
			v...,
		)
	}

	return dbr
}
