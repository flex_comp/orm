module gitlab.com/flex_comp/orm

go 1.17

require (
	gitlab.com/flex_comp/comp v0.1.4
	gorm.io/driver/mysql v1.2.1
	gorm.io/gorm v1.22.4
	gorm.io/plugin/dbresolver v1.1.0
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.3 // indirect
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/spf13/cast v1.4.0 // indirect
	gitlab.com/flex_comp/log v0.1.6 // indirect
	gitlab.com/flex_comp/util v0.0.0-20210729132803-de11a044b5ed // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.18.1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
