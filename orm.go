package orm

import (
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"sync"
)

var (
	ins *Orm
)

func init() {
	ins = new(Orm)
	_ = comp.RegComp(ins)
}

type Orm struct {
	db  *gorm.DB
	mtx sync.Mutex
}

func (o *Orm) Init(map[string]interface{}, ...interface{}) error {
	return nil
}

func (o *Orm) Start(...interface{}) error {
	return nil
}

func (o *Orm) UnInit() {
}

func (o *Orm) Name() string {
	return "orm"
}

func DB() *gorm.DB {
	return ins.DB()
}

func (o *Orm) DB() *gorm.DB {
	return o.db
}

func Reg(models *models) error {
	return ins.Reg(models)
}

func (o *Orm) Reg(models *models) (err error) {
	o.mtx.Lock()
	defer o.mtx.Unlock()

	if o.db == nil {
		o.db, err = gorm.Open(mysql.Open(models.randDsn()), &gorm.Config{})
	}

	if err != nil {
		return err
	}

	err = o.db.Use(
		models.toDBResolver(),
	)

	if err != nil {
		return err
	}

	for dsn, models := range models.data {
		db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
		if err != nil {
			log.Error(err)
			continue
		}

		err = db.AutoMigrate(models...)
		if err != nil {
			log.Error(err)
		}
	}

	return nil
}
